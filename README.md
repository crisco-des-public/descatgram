# Insertion des catégories grammaticales dans le Dictionnaire Électronique des Synonymes (DÉS) 

Ce dépôt est en relation avec le [document de travail halshs-03956407](https://shs.hal.science/halshs-03956407)


Les fichiers au format .csv commençant par CatGramLog donnent la liste des mots présents dans le fichier TLFi de départ qui ne sont pas enregistrés dans le DES.

VerifPermutationsCatGram_20221018.csv donne le nombre de mots trouvés avec les mêmes catégories grammaticales "permutées" : par exemple nous avons 2 mots qui sont adj.;adv. (le ; marque une acception selon le TLFi) et 2 entrées qui sont adv.;adj. On va donc changer adv.;adj. en adj.;adv. de façon à avoir 4 entrées de type adj.;adv. (voir le paragraphe L’homogénéité des codes grammaticaux du document de travail)


catgram_20221108.csv regroupe l'ensemble des codes des catégories grammaticales dans l'ordre alphabétique et séparés, pour certains, par des ; pour les entrées avec acceptions. La seconde colonne donne le nombre d'entrées dans le DES correspondantes.

Enfin, le fichier CatGramErreursAcceptions.csv donne la liste des 844 entrées restantes à traiter considérées à tort comme des acceptions (avec un ; pour séparer les catégories grammaticales alors que ce n'est pas le cas dans le fichier TLFi de départ). 